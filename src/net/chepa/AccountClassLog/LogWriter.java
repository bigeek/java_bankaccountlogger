package net.chepa.AccountClassLog;

import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public final class LogWriter {
  private static volatile LogWriter logClass;
  private final Logger logger = Logger.getLogger(LogWriter.class.getName());
  private FileHandler fh = null;

  private LogWriter() {
    SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");
    try {
      String currentDirectory = System.getProperty("user.dir");
      fh = new FileHandler(currentDirectory + "\\log.log");
    } catch (Exception e) {
      e.printStackTrace();
    }
    fh.setFormatter(new SimpleFormatter());
    logger.addHandler(fh);
  }

  public static LogWriter getInstance() {
    if (logClass == null) {
      synchronized (LogWriter.class) {
        if (logClass == null) {
          logClass = new LogWriter();
        }
      }
    }
    return logClass;
  }

  public void Log(String msg) {
    logger.info(msg);
  }
}
