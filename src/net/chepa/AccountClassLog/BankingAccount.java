package net.chepa.AccountClassLog;

import java.util.Scanner;

public class BankingAccount {
  private static LogWriter logClass;
  private static int lastAccountID = 0;
  private double balance;
  private int accountNumber;

  public BankingAccount(double startBalance) {
    logClass = LogWriter.getInstance();
    balance = startBalance;
    accountNumber = lastAccountID + 1;
    lastAccountID = accountNumber;
  }

  public static void main(String[] args) {

    Scanner in = new Scanner(System.in);

    BankingAccount account1 = new BankingAccount(1000);
    account1.deposit(500);
    account1.withdraw(50);

    BankingAccount account2 = new BankingAccount(0);
    account2.deposit(50);
    account2.withdraw(500);
  }

  public void deposit(double deposit) {
    balance += deposit;
    logClass.Log("Owner of Bank Account #" + accountNumber + " just deposited: " + deposit + " With total of: " + balance);
  }

  public void withdraw(double withdraw) {
    balance -= withdraw;
    logClass.Log("Owner of Bank Account #" + accountNumber + " just withdraw " + withdraw + " With total of: " + balance);
  }

  public int getNumber() {
    return accountNumber;
  }

  public double getBalance() {
    return balance;
  }

}